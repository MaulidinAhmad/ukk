const express = require("express");
const cors = require("cors");
const session = require("express-session");
const fs = require("fs");
const morgan = require("morgan");
// require("dotenv").config();
const moment = require("moment");
const mongoose = require("mongoose");
const hbs = require("hbs");
const path = require("path");
const flash = require("connect-flash");
const helper = require("handlebars-helpers");
const app = express();
const mongoStore = require("connect-mongo")(session);
var port = process.env.PORT || 4000;
const viewPath = path.join(__dirname + "/views");
const partPath = __dirname + "views/layouts/partials";

// Views Handler

var blocks = {};

hbs.registerHelper("extend", function(name, context) {
  var block = blocks[name];
  if (!block) {
    block = blocks[name] = [];
  }

  block.push(context.fn(this)); // for older versions of handlebars, use block.push(context(this));
});

hbs.registerHelper("block", function(name) {
  var val = (blocks[name] || []).join("\n");

  // clear the block
  blocks[name] = [];
  return val;
});
hbs.registerHelper("for", function(from, to, incr, block) {
  var accum = "";
  for (var i = from; i <= to; i += incr) {
    block.data.index = i;
    accum += block.fn(this);
  }
  return accum;
});

hbs.registerHelper("if_eq", function(a, b, opts) {
  if (a == b) {
    return opts.fn(this);
  } else {
    return opts.inverse(this);
  }
});
hbs.registerHelper("formatDate", function(datetime, format) {
  if (moment) {
    // can use other formats like 'lll' too
    format = format;
    return moment(datetime).format(format);
  } else {
    return datetime;
  }
});
const comparison = helper.comparison();
const math = helper.math();
hbs.registerHelper(comparison);
hbs.registerHelper(math);
hbs.registerPartial(
  "navbar",
  fs.readFileSync(__dirname + "/views/layouts/partials/navbar.hbs", "utf8")
);
hbs.registerPartials(partPath);
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(morgan("tiny"));

app.set("views", viewPath);
app.set("view engine", "hbs");
app.set("view options", { layout: "/layouts/app" });
app.set("view data");
app.use(express.static(path.join(__dirname, "public")));

// Mongoose Connection

mongoose.connect(
  "mongodb+srv://ahmad:ahmad123@laundry-ddbo3.mongodb.net/test?ssl=true&retryWrites=true&w=majority" ||
    "mongodb://localhost:27017/ukk",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  }
);

const connection = mongoose.connection;
connection.once("open", () => {
  console.log("database conencted");
});

// Session
app.use(
  session({
    secret: "cendoldawetlimaratusan",
    resave: true,
    saveUninitialized: false,
    expires: new Date(Date.now() + 30 * 86400 * 1000),
    cookie: { maxAge: 12 * 60 * 60 * 1000 },
    store: new mongoStore({
      mongooseConnection: connection,
      clear_interval: 3600
    })
  })
);

app.use(flash());

const route = require("./routes/route");
const auth = require("./routes/auth");
const dashboard = require("./routes/dashboard");
const seed = require("./routes/seed");

app.use(function(req, res, next) {
  res.locals.flashData = req.flash();
  res.locals.session = req.session;
  req.session._garbage = Date();
  req.session.touch();
  next();
});

// Seeder
app.use("/seed", seed);
// Routing
app.use("/", route);
app.use("/auth", auth);
app.use("/dashboard", dashboard);
app.use((req, res, next) => {
  res.status(404);
  if (req.accepts("html")) {
    res.render("404", { layout: "", url: req.url });
  }
});

// Run Server

var server = app.listen(port, "0.0.0.0" || "localhost", () => {
  console.log(`connection run on port ${port}`);
});
