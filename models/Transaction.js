const mongoose = require("mongoose");
const schema = mongoose.Schema;

const Transaction = new schema({
  id_outlet: {
    type: Object,
    required: true
  },
  invoice_code: {
    type: String,
    required: true
  },
  member_id: {
    type: Object,
    required: true
  },
  date: {
    type: Date
  },
  limit_date: {
    type: Date
  },
  paid_date: {
    type: Date
  },
  additional_charge: {
    type: Number
  },
  discount: {
    type: Number
  },
  status: {
    type: String,
    enum: ["baru", "proses", "selesai", "diambil"]
  },
  is_payed: {
    type: String,
    enum: ["dibayar", "belum_dibayar"]
  }
});

module.exports = mongoose.model("transaction", Transaction);
