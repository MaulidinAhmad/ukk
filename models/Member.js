const mongoose = require("mongoose");
const schema = mongoose.Schema;
const dataTables = require("mongoose-datatables");

const Member = new schema({
  memberId: {
    type: Object
  },
  name: {
    type: String,
    maxlength: 100
  },
  address: {
    type: String
  },
  gender: {
    type: String,
    enum: ["L", "P"]
  },
  telephone: {
    type: String
  }
});
Member.plugin(dataTables);

module.exports = mongoose.model("Member", Member);
