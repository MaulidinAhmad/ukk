const mongoose = require("mongoose");
const schema = mongoose.Schema;

const Outlet = new schema({
  name: {
    type: String,
    maxlength: 100
  },
  address: {
    type: String
  },
  telephone: {
    type: String
  }
});

module.exports = mongoose.model("outlet", Outlet);
