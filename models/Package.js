const mongoose = require("mongoose");
const paginate = require("mongoose-paginate-v2");
const schema = mongoose.Schema;

const Package = new schema({
  outlet_id: Object,
  package_name: String,
  type: String,
  price: Number
});
Package.plugin(paginate);
module.exports = mongoose.model("package", Package);
