const mongoose = require("mongoose");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");
const schema = mongoose.Schema;

const transactionDetail = new schema(
  {
    transaction_id: {
      required: true,
      type: Object
    },
    id_paket: {
      type: Object
    },
    qty: {
      type: Number,
      require: true
    },
    description: {
      type: String
    }
  },
  {
    timestamps: true
  }
);
transactionDetail.plugin(aggregatePaginate);

module.exports = mongoose.model("transaction_details", transactionDetail);
