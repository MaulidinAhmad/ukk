const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const dataTables = require("mongoose-datatables");

const userSchema = new Schema(
  {
    name: {
      required: true,
      type: String
    },

    username: {
      type: String,
      require: true
    },
    password: {
      type: String,
      required: true
    },
    id_outlet: {
      type: String
    },
    role: {
      type: String,
      enum: ["admin", "kasir", "owner"]
    }
  },
  {
    timestamps: true
  }
);

userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

userSchema.plugin(dataTables);

module.exports = mongoose.model("Users", userSchema);
