const router = require("express").Router();
const mongoose = require("mongoose");
const User = require("../models/Users");

router.use("/:name/:username/:password", (req, res) => {
  const name = req.params.name;
  const user = req.params.username;
  const pass = req.params.password;
  User.find(
    {
      username: user
    },
    (err, result) => {
      if (err) throw err;
      if (result.length > 0) {
        res.send("Admin Account Already Exist");
      } else {
        const newUser = new User();
        newUser.name = name;
        newUser.username = user;
        newUser.password = newUser.generateHash(pass);
        newUser.id_outlet = mongoose.Types.ObjectId();
        newUser.role = "admin";
        newUser.save(err => {
          res.redirect("/");
        });
      }
    }
  );
});

module.exports = router;
