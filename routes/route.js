const router = require("express").Router();
const Outlet = require("../models/Outlet");

router.route("/").get((req, res, next) => {
  Outlet.find({ _id: req.session.id_outlet }, (err, doc) => {
    res.render("index", { title: "Home", data: doc[0] });
  });
});

module.exports = router;
