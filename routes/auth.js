const router = require("express").Router();
const User = require("../models/Users");
const bcrypt = require("bcrypt");

router.route("/logout").get((req, res) => {
  req.session.destroy(err => {
    if (err) throw err;
    res.redirect("/");
  });
});

router.use((req, res, next) => {
  if (req.session.idUser) {
    res.redirect("/");
  } else {
    next();
  }
});
router.route("/login").get((req, res, next) => {
  res.render("login", { layout: "/layouts/auth/app", title: "Login" });
});

router.route("/login").post((req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  if (!username) {
    req.flash("failed", "Please Input Your Username");
    res.redirect("/auth/login");
  }
  if (!password) {
    req.flash("failed", "Please Input Your Password");
    res.redirect("/auth/login");
  }
  User.find(
    {
      username: username
    },
    (err, data) => {
      if (err) throw err;
      if (data.length < 1) {
        req.flash("failed", "Username Or Password not found");
        res.redirect("/auth/login");
      } else {
        data.forEach(datas => {
          bcrypt.compare(password, datas.password, (err, result) => {
            if (err) throw err;
            if (result) {
              req.session.idUser = datas._id;
              req.session.role = datas.role;
              req.session.id_outlet = datas.id_outlet;
              res.redirect("/dashboard");
            } else {
              req.flash("failed", "Username Or Password not found");
              res.redirect("/auth/login");
            }
          });
        });
      }
    }
  );
});

module.exports = router;
