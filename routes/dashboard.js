const router = require("express").Router();
const pdf = require("html-pdf");
const hbs = require("handlebars");
const helper = require("handlebars-helpers");
const moment = require("moment");
const path = require("path");
const fs = require("fs");
const bcrypt = require("bcrypt");
// import User Model
const User = require("../models/Users");
// import Outlet Model
const Outlet = require("../models/Outlet");
// import member Model
const Member = require("../models/Member");
// import Package Model
const Package = require("../models/Package");
// Import TransactionDetail Model
const transactionDetail = require("../models/TransactionDetail");
// Import Transaction Model
const Transaction = require("../models/Transaction");
const mongoose = require("mongoose");

// middleware
router.use((req, res, next) => {
  if (req.session.idUser) {
    next();
  } else {
    req.flash("failed", "Please Login to Access Dashboard");
    res.redirect("/auth/login");
  }
});

// dashboard Route
router.route("/").get(async (req, res) => {
  const totMem = Member.find({ gender: "L" });

  transactionDetail
    .aggregate([
      {
        $lookup: {
          from: "transactions",
          localField: "transaction_id",
          foreignField: "_id",
          as: "transaction"
        }
      },
      { $unwind: { path: "$transaction", preserveNullAndEmptyArrays: true } },
      {
        $match: {
          $expr: {
            $and: {
              $in: [
                mongoose.Types.ObjectId(req.session.id_outlet),
                ["$transaction.id_outlet"]
              ]
            }
          }
        }
      }
    ])
    .exec((err, countOrder) => {
      Package.find({
        outlet_id: mongoose.Types.ObjectId(req.session.id_outlet)
      }).countDocuments((err, countPackage) => {
        Member.find({}).countDocuments((err, countMember) => {
          res.render("dashboard/dashboard", {
            layout: "dashboard/layouts/app",
            active: "dashboard",
            title: "Dashboard",
            data: {
              order: countOrder.length,
              package: countPackage,
              member: countMember
            }
          });
        });
      });
    });
});
router.route("/getDataChart").get((req, res) => {
  Member.aggregate([
    {
      $facet: {
        totalLaki: [
          {
            $match: {
              gender: {
                $eq: "L"
              }
            }
          },
          {
            $count: "totalLaki"
          }
        ],
        totalPerempuan: [
          {
            $match: {
              gender: {
                $eq: "P"
              }
            }
          },
          { $count: "totalPerempuan" }
        ]
      }
    },
    {
      $project: {
        totalLaki: { $arrayElemAt: ["$totalLaki.totalLaki", 0] },
        totalPerempuan: { $arrayElemAt: ["$totalPerempuan.totalPerempuan", 0] }
      }
    }
  ]).exec((err, ress) => {
    if (err) throw err;
    Transaction.aggregate([
      {
        $facet: {
          dibayar: [
            {
              $match: {
                $and: [
                  {
                    is_payed: {
                      $eq: "dibayar"
                    }
                  },
                  {
                    id_outlet: {
                      $eq: mongoose.Types.ObjectId(req.session.id_outlet)
                    }
                  }
                ]
              }
            },
            {
              $count: "totalDibayar"
            }
          ],
          belumDibayar: [
            {
              $match: {
                $and: [
                  {
                    is_payed: {
                      $eq: "belum_dibayar"
                    }
                  },
                  {
                    id_outlet: {
                      $eq: mongoose.Types.ObjectId(req.session.id_outlet)
                    }
                  }
                ]
              }
            },
            { $count: "totalBelumDibayar" }
          ]
        }
      },
      {
        $project: {
          totalDibayar: { $arrayElemAt: ["$dibayar.totalDibayar", 0] },
          totalBelumDibayar: {
            $arrayElemAt: ["$belumDibayar.totalBelumDibayar", 0]
          }
        }
      }
    ]).exec((err, results) => {
      if (err) throw err;
      res.json({ gender: ress[0], transaction: results[0] });
    });
  });
});

// members Route
router.route("/members").get((req, res) => {
  res.render("dashboard/member/index", {
    layout: "dashboard/layouts/app",
    active: "members",
    title: "Dashboard | Members"
  });
});
router.route("/members/add").get((req, res) => {
  res.render("dashboard/member/add", {
    layout: "dashboard/layouts/app",
    active: "members",
    title: "Dashboard | Members"
  });
});
router.route("/members/add").post((req, res) => {
  const name = req.body.name;
  const address = req.body.address;
  const gender = req.body.gender;
  const telp = req.body.telp;
  if (!name) {
    req.flash("failed", "Please Insert Name");
    res.redirect("/dashboard/members/add");
  }
  if (!address) {
    req.flash("failed", "Please Insert Address");
    res.redirect("/dashboard/members/add");
  }
  if (!gender) {
    req.flash("failed", "Please Insert Gender");
    res.redirect("/dashboard/members/add");
  }
  if (!telp) {
    req.flash("failed", "Please Insert Telephone");
    res.redirect("/dashboard/members/add");
  }

  Member.find({ name: name }, (err, doc) => {
    if (err) throw err;
    if (doc.length > 0) {
      req.flash("failed", "Member Already Exist");
      res.redirect("/dashboard/members/add");
    } else {
      const newMember = new Member();
      newMember.name = name;
      newMember.address = address;
      newMember.gender = gender;
      newMember.telephone = telp;
      newMember.save((err, doc) => {
        if (err) throw err;
        req.flash("success", `Member ${name} Successfully Added`);
        res.redirect("/dashboard/members");
      });
    }
  });
});
router.route("/members/delete/:id").get((req, res) => {
  const id = req.params.id;
  Member.findByIdAndDelete(id, (err, member) => {
    if (err) throw err;
    Transaction.find(
      { member_id: mongoose.Types.ObjectId(id) },
      (err, ress) => {
        if (err) throw err;
        ress.forEach((item, key) => {
          transactionDetail
            .find({
              transaction_id: item._id
            })
            .remove()
            .exec();
        });
        req.flash("success", `Member Success Deleted`);
        res.redirect("/dashboard/members");
      }
    )
      .remove()
      .exec();
  });
});
router.route("/members/edit/:id").get((req, res) => {
  Member.findById(req.params.id, (err, ress) => {
    res.render("dashboard/member/edit", {
      layout: "dashboard/layouts/app",
      active: "members",
      data: ress,
      title: "Dashboard | Members"
    });
  });
});
router.route("/members/update").post((req, res) => {
  const id = req.body.id;
  const name = req.body.name;
  const address = req.body.address;
  const gender = req.body.gender;
  const telp = req.body.telp;

  Member.findByIdAndUpdate(
    id,
    {
      name: name,
      address: address,
      gender: gender,
      telephone: telp
    },
    (err, ress) => {
      if (err) throw err;
      req.flash("success", "Member Success Updated");
      res.redirect("/dashboard/members");
    }
  );
});
router.route("/members/dataTables").post((req, res) => {
  var order = req.body.order[0].column;
  var dir = req.body.order[0].dir;
  var col = req.body.columns[order].data;
  var jsObj = {};
  jsObj[col] = dir;
  Member.dataTables({
    limit: req.body.length,
    skip: req.body.start,
    search: {
      value: req.body.search.value,
      fields: ["address", "name", "telephone", "gender"]
    },
    sort: jsObj,
    formatter: function(user) {
      return {
        name: user.name,
        address: user.address,
        gender: user.gender,
        telp: user.telephone,
        action:
          '<button class="btn btn-danger mr-2" onclick="deleteData(\'/dashboard/members/delete/' +
          user._id +
          "')\">Delete</button>" +
          "</a>" +
          '<a class="btn btn-info" href="/dashboard/members/edit/' +
          user._id +
          '">' +
          "Edit" +
          "</a>"
      };
    }
  }).then(function(table) {
    res.json({
      data: table.data,
      recordsFiltered: table.total,
      recordsTotal: table.total
    });
  });
});

// Outlet Route
router.route("/outlet").get((req, res) => {
  Outlet.find(
    { _id: mongoose.Types.ObjectId(req.session.id_outlet) },
    (err, result) => {
      if (err) throw err;
      res.render("dashboard/outlet", {
        layout: "dashboard/layouts/app",
        active: "outlet",
        title: "Dashboard | Outlet",
        data: result[0]
      });
    }
  );
});
router.route("/outlet/save").post((req, res) => {
  const name = req.body.nama;
  const alamat = req.body.alamat;
  const telp = req.body.telp;

  Outlet.find(
    {
      _id: mongoose.Types.ObjectId(req.session.id_outlet)
    },
    (err, data) => {
      if (err) throw err;
      if (data.length > 0) {
        Outlet.findOneAndUpdate(
          {
            _id: data[0]._id
          },
          { name: name, address: alamat, telephone: telp },
          err => {
            if (err) throw err;
            req.flash("success", "Success Save Outlet");
            res.redirect("/dashboard/outlet");
          }
        );
      } else {
        const newOutlet = new Outlet();
        newOutlet._id = mongoose.Types.ObjectId(req.session.id_outlet);
        newOutlet.name = name;
        newOutlet.address = alamat;
        newOutlet.telephone = telp;
        newOutlet.save((err, data) => {
          if (err) throw err;
          req.flash("success", "Success Save Outlet");
          res.redirect("/dashboard/outlet");
        });
      }
    }
  );
});

// Package Route
router.route("/package/:pages?*/list/:search?*").get((req, res) => {
  function titleCase(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(" ");
  }

  let query = {};
  let page = req.params.pages;
  let search = req.query.search;
  if (!page) {
    page = 1;
  }

  if (search) {
    query = {
      $and: [
        { outlet_id: mongoose.Types.ObjectId(req.session.id_outlet) },
        { package_name: { $regex: titleCase(search) } }
      ]
    };
  } else {
    query = { outlet_id: mongoose.Types.ObjectId(req.session.id_outlet) };
  }

  Package.paginate(query, { page: page, limit: 4 }, (err, ress) => {
    res.render("dashboard/package/index", {
      layout: "dashboard/layouts/app",
      active: "package",
      title: "Dashboard | Package",
      paginate: ress
    });
  });
});
router.route("/package/add").get((req, res) => {
  res.render("dashboard/package/add", {
    layout: "dashboard/layouts/app",
    active: "package",
    title: "Dashboard | Package"
  });
});
router.route("/package/add").post((req, res) => {
  const type = req.body.type;
  const name = req.body.name;
  const price = req.body.price;
  if (!type) {
    req.flash("failed", "Please fill your type");
    res.redirect("/dashboard/package/add");
  }
  if (!name) {
    req.flash("failed", "Please fill your name");
    res.redirect("/dashboard/package/add");
  }
  if (!price) {
    req.flash("failed", "Please fill your price");
    res.redirect("/dashboard/package/add");
  }
  function titleCase(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(" ");
  }
  Package.find(
    {
      package_name: name
    },
    (err, ress) => {
      if (err) throw err;
      if (ress.length > 0) {
        req.flash("failed", "Package Already Exist");
        res.redirect("/dashboard/package/add");
      } else {
        const newPackage = new Package();
        newPackage.outlet_id = mongoose.Types.ObjectId(req.session.id_outlet);
        newPackage.package_name = titleCase(name);
        newPackage.type = type;
        newPackage.price = price;
        newPackage.save((err, ress) => {
          if (err) throw err;
          req.flash("success", "Package Success Added");
          res.redirect("/dashboard/package/list");
        });
      }
    }
  );
});
router.route("/package/delete/:id").get((req, res) => {
  Package.findByIdAndDelete({ _id: req.params.id }, (err, package) => {
    if (err) throw err;
    transactionDetail
      .find({ id_paket: package._id }, (err, ress) => {
        if (err) throw err;
        ress.forEach((item, index) => {
          Transaction.find({ _id: item.transaction_id })
            .remove()
            .exec();
        });
        req.flash("success", `Package Success Deleted`);
        res.redirect("/dashboard/package/list");
      })
      .remove()
      .exec();
  });
});
router.route("/package/edit/:id").get((req, res) => {
  Package.findById(req.params.id, (err, ress) => {
    if (err) throw err;
    res.render("dashboard/package/edit", {
      layout: "dashboard/layouts/app",
      active: "package",
      title: "Dashboard | Package",
      data: ress
    });
  });
});
router.route("/package/update").post((req, res) => {
  const id = req.body.id;
  const type = req.body.type;
  const name = req.body.name;
  const price = req.body.price;

  if (!type) {
    req.flash("failed", "Please fill your type");
    res.redirect("/dashboard/package/add");
  }
  if (!name) {
    req.flash("failed", "Please fill your name");
    res.redirect("/dashboard/package/add");
  }
  if (!price) {
    req.flash("failed", "Please fill your price");
    res.redirect("/dashboard/package/add");
  }
  if (!id) {
    req.flash("failed", "Please fill your id");
    res.redirect("/dashboard/package/add");
  }

  Package.findByIdAndUpdate(
    id,
    {
      package_name: name,
      type: type,
      price: price
    },
    (err, ress) => {
      if (err) throw err;
      req.flash("success", "Package Success Updated");
      res.redirect("/dashboard/package/list");
    }
  );
});

// Users
router.route("/users").get((req, res) => {
  res.render("dashboard/user/index", {
    layout: "dashboard/layouts/app",
    active: "users",
    title: "Dashboard | Users"
  });
});
router.route("/users/add").get((req, res) => {
  res.render("dashboard/user/add", {
    layout: "dashboard/layouts/app",
    active: "users",
    title: "Dashboard | Users"
  });
});
router.route("/users/edit/:id").get((req, res) => {
  User.findById(req.params.id, (err, docs) => {
    res.render("dashboard/user/edit", {
      layout: "dashboard/layouts/app",
      active: "users",
      title: "Dashboard | Users",
      data: docs
    });
  });
});
router.route("/users/add").post((req, res) => {
  const name = req.body.name;
  const username = req.body.username;
  const password = req.body.password;
  const role = req.body.role;

  if (!name) {
    req.flash("failed", "Please Input Your Name");
    res.redirect("/dashboard/users/add");
  }

  if (!username) {
    req.flash("failed", "Please Input Your Username");
    res.redirect("/dashboard/users/add");
  }
  if (!password) {
    req.flash("failed", "Please Input Your Password");
    res.redirect("/dashboard/users/add");
  }
  if (!role) {
    req.flash("failed", "Please Input Your Role");
    res.redirect("/dashboard/users/add");
  }
  User.find(
    {
      username: username
    },
    (err, prevData) => {
      if (err) {
        throw err;
      }
      if (prevData.length > 0) {
        req.flash("failed", "Username Already Takken");
        res.redirect("/dashboard/users/add");
      } else {
        const newUser = new User();
        newUser.name = name;
        newUser.username = username;
        newUser.password = newUser.generateHash(password);
        newUser.id_outlet = mongoose.Types.ObjectId(req.session.id_outlet);
        newUser.role = role;
        newUser.save().then(() => {
          req.flash("success", "User Success Created");
          res.redirect("/dashboard/users");
        });
      }
    }
  );
});
router.route("/users/update").post((req, res) => {
  const id = req.body.id;
  const name = req.body.name;
  const username = req.body.username;
  const password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
  const role = req.body.role;
  if (!name) {
    req.flash("failed", "Please Input Your Name");
    res.redirect("/dashboard/users/edit/" + id);
  }

  if (!username) {
    req.flash("failed", "Please Input Your Username");
    res.redirect("/dashboard/users/edit/" + id);
  }
  if (!password) {
    req.flash("failed", "Please Input Your Password");
    res.redirect("/dashboard/users/edit/" + id);
  }
  if (!role) {
    req.flash("failed", "Please Input Your Role");
    res.redirect("/dashboard/users/edit" + id);
  }
  User.findByIdAndUpdate(
    id,
    { name: name, username: username, password: password, role: role },
    (err, doc) => {
      if (err) throw err;
      req.flash("success", "Success Update User");
      res.redirect("/dashboard/users");
    }
  );
});
router.route("/users/delete/:id").get((req, res) => {
  const id = req.params.id;
  User.findByIdAndDelete(id, (err, ress) => {
    if (err) throw err;
    req.flash("success", `User ${ress.name} Success Deleted`);
    res.redirect("/dashboard/users");
  });
});

router.route("/users/dataTables").post((req, res) => {
  var order = req.body.order[0].column;
  var dir = req.body.order[0].dir;
  var col = req.body.columns[order].data;
  var jsObj = {};
  jsObj[col] = dir;
  User.dataTables({
    limit: req.body.length,
    skip: req.body.start,
    search: {
      value: req.body.search.value,
      fields: ["username", "name", "role"]
    },
    find: {
      role: ["owner", "kasir"],
      id_outlet: req.session.id_outlet
    },
    sort: jsObj,
    formatter: function(user) {
      return {
        name: user.name,
        username: user.username,
        role: user.role,
        action:
          '<button class="btn btn-danger mr-2" onclick="deleteData(\'/dashboard/users/delete/' +
          user._id +
          "')\">Delete</button>" +
          "</a>" +
          '<a class="btn btn-info" href="/dashboard/users/edit/' +
          user._id +
          '">' +
          "Edit" +
          "</a>"
      };
    }
  }).then(function(table) {
    res.json({
      data: table.data,
      recordsFiltered: table.total,
      recordsTotal: table.total
    });
  });
});
// Orders
router.route("/orders/:page?*/list").get((req, res) => {
  let page = req.params.page;
  if (!page) {
    page = 1;
  }
  let myAgregate = transactionDetail.aggregate([
    {
      $lookup: {
        from: "transactions",
        localField: "transaction_id",
        foreignField: "_id",
        as: "transaction"
      }
    },
    { $unwind: { path: "$transaction", preserveNullAndEmptyArrays: true } },
    {
      $lookup: {
        from: "members",
        localField: "transaction.member_id",
        foreignField: "_id",
        as: "transactionMember"
      }
    },
    { $unwind: "$transactionMember" },
    {
      $lookup: {
        from: "packages",
        localField: "id_paket",
        foreignField: "_id",
        as: "package"
      }
    },
    { $unwind: "$package" },
    {
      $match: {
        $expr: {
          $and: {
            $in: [
              mongoose.Types.ObjectId(req.session.id_outlet),
              ["$transaction.id_outlet"]
            ]
          }
        }
      }
    },
    { $sort: { "transaction.date": -1 } }
  ]);
  transactionDetail.aggregatePaginate(
    myAgregate,
    { page: page, limit: 4 },
    (err, ress) => {
      if (err) throw err;
      res.render("dashboard/order/index", {
        layout: "dashboard/layouts/app",
        active: "orders",
        title: "Dashboard | Orders",
        paginate: ress
      });
    }
  );
});

router.route("/orders/add").get((req, res) => {
  res.render("dashboard/order/add", {
    layout: "dashboard/layouts/app",
    active: "orders",
    title: "Dashboard | Orders"
  });
});
router.route("/orders/add").post((req, res) => {
  const member = req.body.member;
  const limit_date = req.body.limit_date;
  const package = req.body.package;
  const qty = req.body.qty;
  const desc = req.body.description;
  function makeid(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  let nowDate = new Date().toDateString();
  const newTransaction = new Transaction();
  newTransaction.id_outlet = mongoose.Types.ObjectId(req.session.id_outlet);
  newTransaction.invoice_code = "INV_" + makeid(5);
  newTransaction.limit_date = limit_date;
  newTransaction.member_id = mongoose.Types.ObjectId(member);
  newTransaction.date = new Date();
  newTransaction.paid_date = "";
  newTransaction.additional_charge = 0.01;
  if (qty >= 5) {
    newTransaction.discount = 0.05;
  } else {
    newTransaction.discount = 0;
  }
  newTransaction.status = "baru";
  newTransaction.is_payed = "belum_dibayar";
  newTransaction.save((err, ress) => {
    if (err) throw err;
    const newTransactionDetail = new transactionDetail();
    newTransactionDetail.transaction_id = newTransaction._id;
    newTransactionDetail.id_paket = mongoose.Types.ObjectId(package);
    newTransactionDetail.qty = qty;
    newTransactionDetail.description = desc;
    newTransactionDetail.save(err => {
      if (err) throw err;
      req.flash("success", "Order Success Added");
      res.redirect("/dashboard/orders/list");
    });
  });
});
router.route("/orders/edit/:id").get((req, res) => {
  const id = req.params.id;
  transactionDetail
    .aggregate([
      { $match: { _id: mongoose.Types.ObjectId(id) } },
      {
        $lookup: {
          from: "transactions",
          localField: "transaction_id",
          foreignField: "_id",
          as: "transaction"
        }
      },
      { $unwind: "$transaction" },
      {
        $lookup: {
          from: "members",
          localField: "transaction.member_id",
          foreignField: "_id",
          as: "transactionMember"
        }
      },
      { $unwind: "$transactionMember" },
      {
        $lookup: {
          from: "packages",
          localField: "id_paket",
          foreignField: "_id",
          as: "package"
        }
      },
      { $unwind: "$package" }
    ])
    .exec((err, ress) => {
      if (err) throw err;
      res.render("dashboard/order/edit", {
        layout: "dashboard/layouts/app",
        active: "orders",
        title: "Dashboard | Orders",
        data: ress[0]
      });
    });
});
router.route("/orders/update").post((req, res) => {
  const id = req.body.id;
  const member = req.body.member;
  const limit_date = req.body.limit_date;
  const package = req.body.package;
  const qty = req.body.qty;
  const desc = req.body.description;
  let nowDate = new Date().toDateString();
  const status = req.body.status;
  let discount = 0;
  const is_payed = req.body.is_payed;
  let payed_date = "";
  if (is_payed == "dibayar") {
    payed_date = new Date();
  }
  if (qty >= 5) {
    discount = 0.05;
  }

  transactionDetail.findById(id, (err, ress) => {
    if (err) throw err;
    transactionDetail.findByIdAndUpdate(
      ress._id,
      {
        id_paket: mongoose.Types.ObjectId(package),
        qty: qty,
        description: desc
      },
      err => {
        if (err) throw err;
        Transaction.findByIdAndUpdate(
          ress.transaction_id,
          {
            limit_date: limit_date,
            member_id: mongoose.Types.ObjectId(member),
            paid_date: payed_date,
            additional_charge: 0.01,
            discount: discount,
            status: status,
            is_payed: is_payed
          },
          err => {
            if (err) throw err;
            req.flash("success", "Success Update Orders");
            res.redirect("/dashboard/orders/list");
          }
        );
      }
    );
  });
});
router.route("/orders/delete/:id").get((req, res) => {
  const id = req.params.id;
  transactionDetail.findByIdAndDelete(id, (err, ress) => {
    if (err) throw err;
    Transaction.findByIdAndDelete(ress.transaction_id, (err, ress) => {
      if (err) throw err;
      req.flash("success", "Transaction Success Deleted");
      res.redirect("/dashboard/orders/list");
    });
  });
});

// router.route("/orders/invoice/:id").get((req, res) => {
//   const id = req.params.id;
//   transactionDetail
//     .aggregate([
//       { $match: { _id: mongoose.Types.ObjectId(id) } },
//       {
//         $lookup: {
//           from: "transactions",
//           localField: "transaction_id",
//           foreignField: "_id",
//           as: "transaction"
//         }
//       },
//       { $unwind: "$transaction" },
//       {
//         $lookup: {
//           from: "members",
//           localField: "transaction.member_id",
//           foreignField: "_id",
//           as: "transactionMember"
//         }
//       },
//       { $unwind: "$transactionMember" },
//       {
//         $lookup: {
//           from: "packages",
//           localField: "id_paket",
//           foreignField: "_id",
//           as: "package"
//         }
//       },
//       { $unwind: "$package" }
//     ])
//     .exec((err, ress) => {
//       if (err) throw err;
//       Outlet.findById(req.session.id_outlet, (err, outlet) => {
//         if (err) throw err;
//         User.findById(req.session.idUser)
//           .select("-password")
//           .exec((err, users) => {
//             res.render("dashboard/order/invoice", {
//               layout: "",
//               active: "orders",
//               title: "Orders | Invoice",
//               data: ress[0],
//               outlet: outlet,
//               user: users
//             });
//           });
//       });
//     });
// });
router.route("/orders/getMember").post((req, res) => {
  Member.find({}, (err, ress) => {
    if (err) throw err;
    res.send(ress);
  });
});
router.route("/orders/getPackage").post((req, res) => {
  Package.find(
    {
      outlet_id: mongoose.Types.ObjectId(req.session.id_outlet)
    },
    (err, ress) => {
      if (err) throw err;
      res.send(ress);
    }
  );
});

// Report
router.route("/report").get((req, res) => {
  Outlet.findById(req.session.id_outlet).countDocuments((err, totals) => {
    if (err) throw err;
    res.render("dashboard/report/index", {
      layout: "dashboard/layouts/app",
      active: "report/repor",
      title: "Dashboard | Report",
      outlet: totals
    });
  });
});
router.route("/report/html").post((req, res) => {
  let range = new String(req.body.date);
  range = range.split(" - ");
  const rangeFrom = new Date(range[0]);
  let rangeTo = new Date(range[1] + "T23:59:59");
  transactionDetail.aggregate(
    [
      {
        $lookup: {
          from: "transactions",
          localField: "transaction_id",
          foreignField: "_id",
          as: "transaction"
        }
      },
      { $unwind: { path: "$transaction", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "members",
          localField: "transaction.member_id",
          foreignField: "_id",
          as: "transactionMember"
        }
      },
      { $unwind: "$transactionMember" },
      {
        $lookup: {
          from: "packages",
          localField: "id_paket",
          foreignField: "_id",
          as: "package"
        }
      },
      { $unwind: "$package" },
      {
        $match: {
          $expr: {
            $and: [
              {
                $gte: ["$transaction.date", rangeFrom]
              },
              {
                $lte: ["$transaction.date", rangeTo]
              },
              {
                $in: [
                  mongoose.Types.ObjectId(req.session.id_outlet),
                  ["$transaction.id_outlet"]
                ]
              }
            ]
          }
        }
      }
    ],
    (err, ress) => {
      if (err) throw err;
      Outlet.find(
        { _id: mongoose.Types.ObjectId(req.session.id_outlet) },
        (err, docs) => {
          res.render("dashboard/report/html", {
            layout: "",
            data: ress,
            tanggal: req.body.date,
            outlet: docs[0].name,
            title: "Report"
          });
        }
      );
    }
  );
});
router.route("/report/pdf").post((req, res) => {
  let range = new String(req.body.date);
  range = range.split(" - ");
  const rangeFrom = new Date(range[0]);
  let rangeTo = new Date(range[1] + "T23:59:59");
  transactionDetail.aggregate(
    [
      {
        $lookup: {
          from: "transactions",
          localField: "transaction_id",
          foreignField: "_id",
          as: "transaction"
        }
      },
      { $unwind: { path: "$transaction", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "members",
          localField: "transaction.member_id",
          foreignField: "_id",
          as: "transactionMember"
        }
      },
      { $unwind: "$transactionMember" },
      {
        $lookup: {
          from: "packages",
          localField: "id_paket",
          foreignField: "_id",
          as: "package"
        }
      },
      { $unwind: "$package" },
      {
        $match: {
          $expr: {
            $and: [
              {
                $gte: ["$transaction.date", rangeFrom]
              },
              {
                $lte: ["$transaction.date", rangeTo]
              },
              {
                $in: [
                  mongoose.Types.ObjectId(req.session.id_outlet),
                  ["$transaction.id_outlet"]
                ]
              }
            ]
          }
        }
      }
    ],
    (err, ress) => {
      Outlet.find(
        { _id: mongoose.Types.ObjectId(req.session.id_outlet) },
        (err, docs) => {
          let templeteHtml = fs.readFileSync(
            path.join(process.cwd(), "/views/dashboard/report/template.html"),
            "utf8"
          );
          let data = { ress, outlet: docs[0].name, tanggal: req.body.date };
          hbs.registerHelper("formatDate", function(datetime, format) {
            if (moment) {
              // can use other formats like 'lll' too
              format = format;
              return moment(datetime).format(format);
            } else {
              return datetime;
            }
          });
          const comparison = helper.comparison();
          const math = helper.math();
          let template = hbs.compile(templeteHtml);
          hbs.registerHelper(comparison);
          hbs.registerHelper(math);
          let html = template(data);
          var options = { format: "Letter" };

          pdf.create(html, options).toStream(function(err, stream) {
            if (err) throw err;

            const pdfName = req.body.date + ".pdf";
            res.setHeader("Content-Type", "application/pdf");
            res.setHeader(
              "Content-Disposition",
              "attachment; filename=" + pdfName + ";"
            );
            // res.end(buffer, "binary");
            stream.pipe(res);
          });
        }
      );
    }
  );
});

module.exports = router;
